<?php

require_once '../config.php';
require_once '../app/helpers/database.php';

use app\helpers\database;

// Créez un script PHP à exécuter en ligne de commande (CLI) permettant de migrer (importer dans une base de données) le(s) fichier(s) SQL présent(s) dans un dossier d'une application MVC.

//À l'exécution de ce script, le code SQL présent dans le(s) fichier(s) migré(s) doit être exécuté sur le serveur SQL lié à l'application.

//Le script doit comporter au moins un paramètre, le nom du fichier à importer.


$migrationDir = ROOT_PATH . '/migrations/';

if ($argc != 2) {
echo "Utilisation: php migrate.php <course.sql><br>";
    exit(1);
    }


$filename = $argv[1];
$file = $migrationDir . $filename;


if (!file_exists($file)) {
echo "Le fichier $filename n'existe pas dans le dossier de migrations.<br>";
exit(1);
}

$db = database::connect();


echo "Migration du fichier $filename...<br>";
$sql = file_get_contents($file);
$db->exec($sql);
echo "Migration terminée.<br>";

echo "La migration du fichier $filename a été exécutée avec succès.<br>";