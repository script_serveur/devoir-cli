<?php

namespace app\models;
use app\helpers\database;
class model
{
    public function getAll($table): array
    {
        $connect = database::connect();
        $request = $connect->query("SELECT * FROM $table");
        $rows = [];
        while ($data = $request->fetchObject()) {
            $rows[] = $data;
        }
        return $rows;
    }
}