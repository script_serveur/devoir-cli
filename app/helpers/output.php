<?php

namespace app\helpers;

abstract class output
{

    /**
     * @param string $view
     * @param object|null $data
     * @return void
     */
    public static function getContent(string $view, object $data = null): void
    {
        foreach (VIEWS_EXT as $ext) {
            $complete_path = ROOT_PATH . '/views/' . $view . '.' . $ext;
            if (file_exists($complete_path)) {
                // php include (include_once avoid multiple includes)
                include_once $complete_path;
            }
        }
    }

    /**
     * @param string $view
     * @param object $data
     * @return void
     */
    public static function renderView(string $view, object $data): void
    {
        self::getContent($view, $data);
    }

    public static function render(string $view, mixed $data): void
    {
        echo self::$view($data);
    }

    public static function courseslist(mixed $data, array $columns)
    {
        echo self::table($data, $columns);
    }

    public static function table(array|object $data, array|object $cols, string $title = '', string $class = ''): string
    {
        $thead = '';
        $tbody = '';
        if (is_array($cols) || is_object($cols)) {
            foreach ($cols as $th) {
                $thead .= '<th>' . $th . '</th>';
            }
        } else {
            $thead = '<th>' . $cols . '</th>';
        }
        if (is_array($data) || is_object($data)) {
            foreach ($data as $sub) {
                $tbody .= '<tr>';
                if (is_array($sub) || is_object($sub)) {
                    foreach ($sub as $value) {
                        $tbody .= '<td>' . $value . '</td>';
                    }
                } else {
                    $tbody .= '<td>' . $sub . '</td>';
                }
                $tbody .= '</tr>';
            }
        } else {
            $tbody .= '<tr><td>' . $data . '</td></tr>';
        }

        return '<h2>' . $title . '</h2><table class="table table-striped ' . $class . '"><thead><tr>' . $thead . '</tr></thead><tbody>' . $tbody . '</tbody></table>';
    }
}