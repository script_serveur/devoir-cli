<?php

namespace app\helpers;

abstract class routing
{
    public static function routing(string $view)
    {
        $route = parse_url($_SERVER['REQUEST_URI']);
        if (str_contains($route['query'], 'view=api/')) {
            $params = [];
            $elements = explode('/', rtrim($route['query'], '/'));
            foreach ($elements as $key => $value) {
                if ($key == 1) {
                    $class = 'app\Controllers\\' . ucfirst($value);
                } elseif ($key == 2) {
                    $method = $value;
                } elseif ($key == 0) {
                    continue;
                } else {
                    $params[] = $value;
                }
            }
            if (!empty($class) && !empty($method)) {
                // Reflection API : https://www.php.net/manual/fr/book.reflection.php
                $r = new \ReflectionMethod($class, $method);
                $nbr = $r->getNumberOfParameters();
                if ($nbr != count($params)) {
                    // php core Exception
                    throw new \Exception('Parameters count mismatch');
                }
                // class instantiation (class name can be a variable)
                $controller = new $class();
                // check if method exists
                is_callable($method, true, $callable_name);
                // method call with parameters (specific php syntax)
                $controller->{$callable_name}(...array_values($params));
            }
        } elseif (!empty($view)) {
            output::getContent($view);
        } else {
            // redirection
            header('Location: index.php?view=view/body');
            die;
        }
    }
}