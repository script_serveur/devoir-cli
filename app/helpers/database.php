<?php

namespace app\helpers;

use PDO;

class database
{
    public static function connect()
    {
        global $connect;

        if (is_a($connect, 'PDO')) {
            return $connect;
        } else {
            try {
                $connect = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8', DB_USER, DB_PASSWORD);
            } catch (PDOException $exception) {
                die ('Erreur: ' . $exception->getMessage());
            }
            return $connect;
        }
    }
}